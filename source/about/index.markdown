---
layout: page
title: About
footer: false
comments: false
sidebar: false
---

This is Hong Xu's personal technology blog.

My personal page on Github: [https://github.com/xuhdev](https://github.com/xuhdev)

My personal page on Bitbucket.org: [https://bitbucket.org/xuhdev](https://bitbucket.org/xuhdev)

My Twitter account: [http://twitter.com/#!/xuhdev](http://twitter.com/#!/xuhdev)


Email: [hong@topbug.net](mailto:hong@topbug.net "Hong Xu <hong@topbug.net>") (Preferred) OR [xuhdev@gmail.com](mailto:xuhdev@gmail.com "Hong Xu <xuhdev@gmail.com>")


_* The Twitter and Email social icons are part of the Social Network Icon Pack
by [Komodo Media, Rogie King](http://komodomedia.com/), which is licensed under
a
[Creative Commons Attribution-Share Alike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/).
The GitHub icon is made by [Burin Asavesna](http://helloburin.com/)._
